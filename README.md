README

# OVERVIEW
This tool helps students forumulate better search queries.

# EXAMPLE
http://dev0.library.wwu.edu/rws/entity-extraction.php

# TODO:
 - add a chart showing result count of each search query variant (to show the sweet spot)
 - make the entities drag and drop rather than check-boxes, perhaps using http://interactjs.io/  
