<?php

require("./api-keys.php");

if (isset($_REQUEST['words'])) {
	$words = trim($_REQUEST['words']);
	$words = filter_var($words, FILTER_SANITIZE_STRING);
} else {
	echo "Error - missing input";
	exit();
}


$chart = "";
$labels = "";
$chart_data = "";

if ($words) {
	$words_encoded = urlencode($words);

	# now let's use the text categorization api to identify what these words relate to
	$url = 'https://api.dandelion.eu/datatxt/cl/v1/?text=' . $words_encoded . '&model=48979eda-ed66-4e24-bb66-f329efb99118&$app_id=' . $APP_ID . '&$app_key=' . $APP_KEY; 

	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
	$data = curl_exec($curlSession);
	curl_close($curlSession);

	$json_array = json_decode($data, true);

	// {"error":true,"status":401,"code":"error.authenticationError","message":"usage limits are exceeded","data":{}}

	$error = $json_array["error"];
	if ($error) {
		$error_msg = $json_array["message"];
		echo $error_msg;
		exit();
	}

	$categories = $json_array["categories"];


	if ($categories) {

		/*
		echo "<pre>";
		print_r($categories);
		echo "</pre>";
		*/

		function so ($a, $b) { 
			return (strcmp ($a['name'],$b['name']));    
		}

		usort($categories, 'so');

		/*
		echo "<hr><pre>";
		print_r($categories);
		echo "</pre>";
		*/

		$output_json = json_encode($categories);
		echo $output_json;

	}

}

?>