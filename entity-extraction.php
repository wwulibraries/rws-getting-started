<?php

require("./api-keys.php");

# also consider http://apidemo.theysay.io/

$onesearch = "http://library.wwu.edu/onesearch/";

echo '
<!html>
<head>
	<title>Entity Extraction Tool</title>
	<link href="grid.css" rel="stylesheet" type="text/css">
	<link href="style.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

</head>
<body>
';

if (isset($_POST['confidence'])) {
	$confidence_form = intval($_POST['confidence']);
} else {
	$confidence_form = "50";
}

	$names = [];

	# get list of categories from model.json
	$json = file_get_contents('./model.json');
	$json_a = json_decode($json, true);
	$categories = $json_a["categories"];
	foreach ($categories as $category) {
		$names[] = $category["name"];
	}

	# sort($names);		// put them in alphabetical order;
	$sizeof_names = sizeof($names);
	foreach ($names as $name) {
		$temp_data[] = 0;
	}


if (isset($_POST['text'])) {
	$text = trim($_POST['text']);
	$text = filter_var($text, FILTER_SANITIZE_STRING);

	$text_encoded = urlencode($text);
	$confidence = intval($_POST['confidence']) / 100;

	$url = 'https://api.dandelion.eu/datatxt/nex/v1/?min_confidence=' . $confidence . '&text=' . $text_encoded . '&include=types%2Cabstract&$app_id=' . $APP_ID . '&$app_key=' . $APP_KEY; 
	#	echo $url;

	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
	$data = curl_exec($curlSession);
	# curl_close($curlSession);
	$json_array = json_decode($data, true);

	$error = $json_array["error"];
	if ($error) {
		$error_msg = $json_array["message"];
		echo $error_msg;
		exit();
	}


	$annotations = $json_array["annotations"];


	$counter = 0;
	$output = "<div class='Grid-cell'><ul id='extracted-entities'>";
	$words = "";
	$chart = "";
	$labels = "";
	$chart_data = "";

	if ($annotations) {
		foreach($annotations as $annotation) {
			$word = htmlentities($annotation["spot"]);
			$words .= "+" . $word;
			$abstract = $annotation["abstract"];
			$link = $onesearch . $word;

			$type_label = "";
			$types = $annotation["types"];
			foreach ($types as $key => $value) {
				$label = str_replace("http://dbpedia.org/ontology/", "", $value);
				$type_label .= " " . $label;
			}

			$output .= "<li><input type='checkbox' class='word-checkbox' name='check" . $counter . "' id='check" . $counter . "'> <a target='" . $link . "'' href='" . $link . "' title='" . $abstract . "'>" . $word . "</a> </li>";
			$counter++;
		}	

		$words_encoded = urlencode($words);

	}
	$output .= "</ul></div>";
}


?>
<form method="post" id="theForm">
	<div id="header">
		<div class="Grid">
			  <div class="Grid-cell  u-2of3"> 
				<textarea name="text" id="text" placeholder="Text to analyze"><?php echo $text; ?></textarea>
			</div>
			<div class="Grid-cell">
			  	More Tags <input type="range" size="3" name="confidence" id="confidence" min="0" max="100" step="10" value="<?php echo $confidence_form; ?>"> More Precision
			  	&nbsp;
				<input type=submit name="Submit" id="Submit">
			</div>
		</div>
		<div class="Grid">
			<div class="Grid-cell"><input type="checkbox" id="check-all"> &nbsp;  Entities</div>
			<div class="Grid-cell">Related Subjects</div>
			<div class="Grid-cell u-2of3">Search History
				<span class="right"><a class="white-text" href="https://dandelion.eu" target="dandelion">Powered by Dandelion API</a> &nbsp; </span>
			</div>
		</div>
	</div>
</form>

	<div class="Grid results">
		<?php echo $output; ?>
		<div class='Grid-cell' id='facets'>  </div>
		<div class='Grid-cell  u-2of3' id='search-history' data-max-count="0"> 

			<div style="width:90%" id="canvas-parent">
				<canvas id="canvas" height="250" width="450"></canvas>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class='Grid'>
			<div class='Grid-cell  u-2of3'><input type='text' id='query' name='query'></div>
			<div class='Grid-cell'><a href id='goToOneSearch'><img src='http://dev0.library.wwu.edu/primo/images/OneSearch-logo.png'></a></div>
		</div>
	</div>
	<script src="scope-count.js"></script>

   <script>

     $(window).load(function() {
            window.radarChartData = {
                labels: ['<?php echo implode("','", $names); ?>'],
                datasets: [
                    {
                        label: "My  dataset",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [<?php echo implode(",", $temp_data); ?>]
                    }
                ]
            };

            var options = { 
                responsive: true,
                maintainAspectRatio: true
            }

            window.radarContext = document.getElementById("canvas").getContext("2d");
            window.myRadar = new Chart(radarContext).Radar(radarChartData, options);
     });
</script>




</body>
</html>