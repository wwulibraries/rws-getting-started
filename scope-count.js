
// Author: David . Bass @ w w u . e d u  
// 2016 
// Purpose: this script  adds estimated result counts to the list of scopes (when they are presented as tabs)

 "use strict";

var scopeTabs = (function() {

    $(document).ready(function() {

        var vid = "WWU";
        var institution = "WWU";
        var tab_count_url = "./hit-count.php";
        var scopes = {
                'a':'atwwu', 
                'b': 'wwusummit', 
                'c': 'everything'
        };      

        $("#confidence").on("change", function() {
            $("#theForm").submit();
        });

        $("#text").on("blur", function() {
            $("#theForm").submit();
        });

        function getCount(tabId, combined_query, scope, institution) {
            var api_query = "&q=any,contains," + encodeURIComponent(combined_query);
            var ajax_query = api_query.replace(/%2C/g, '+');       // replace comma with plus
            var count_url = tab_count_url + "?i=" + institution + "&s=" + scope + ajax_query + "&callback=?";
            var thisId = "#" + tabId;

            $(thisId).append(" <span class='spin-container'> &nbsp; &nbsp; <span class='spin'> &nbsp; </span> </span>");

            $.getJSON(count_url, function(data) {
                $("#facets").html(data[1]);

                //                $("#search-history").data("count",data[0]);
                var count = data[0];
                add_history(combined_query, count);

            }).done(function( json, data ) {
                // return data.count;
            }).fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
            });
        }


        $(document).on("change", ".facet-checkboxes", function() {
            var combined_query =  $("#query").val();
            $(".facet-checkboxes").each(function() {
                var isChecked = $(this).prop("checked");
                var thisQuery = $(this).val();

                var now = Date.now();
                var thisQueryLink = "<a target='" + now + "' href='http://library.wwu.edu/onesearch/" + thisQuery + "'>" + thisQuery + "</a>";
                var this_checkbox_and_text = "<li class='via-facet'> <input type=checkbox CHECKED class='word-checkbox'> " + thisQueryLink + "</li>";
                if (isChecked) {
                    combined_query += " " + thisQuery;
                    $("#extracted-entities").append(this_checkbox_and_text);
                }
            });

            $("#query").val(combined_query);
            query_search(); 
        });


        $(document).on("change", ".word-checkbox", function() {
            var combined_query = "";
            $(".word-checkbox").each(function() {
                var isChecked = $(this).prop("checked");
                var thisQuery = $(this).text();
                if (isChecked) {
                    combined_query += " " + $(this).next().text();
                }
            });

            $("#query").val(combined_query);
            query_search(); 
        });

        $(document).on("change", "#query", function() {
            query_search();            
        });

        $(document).on("change", "#check-all", function() {
                var allChecked = $(this).prop("checked");
                if (allChecked === true) {
                    // check all of the checkboxes;
                    $(".word-checkbox").prop("checked", true);
                } else {
                    $(".word-checkbox").prop("checked", false);
                }
        });



        function query_search() {
            var now = Date.now();
            var combined_query = $("#query").val();
            var out_link = "http://library.wwu.edu/onesearch/" + combined_query;
            $("#goToOneSearch").attr("href", out_link);
            $("#goToOneSearch").attr("target", now);
            var target_id = "";

            $(".query_results").html("");
            getCount(target_id, combined_query, 'everything', institution);
        }

        function add_history(combined_query, count) {
            var denominator;
            var now = Date.now();
            var max_count = $("#search-history").data("max-count");
            if (count > max_count) {
                $("#search-history").data("max-count", count);
                 denominator = count;
            } else {
                denominator = max_count;
            }

            var thisQueryLink = "<a target='" + now + "' href='http://library.wwu.edu/onesearch/" + combined_query + "'>" + combined_query + "</a>";
            var output =  "<div class='Grid'><div class='Grid-cell'>" + thisQueryLink + "</div>";
             output += "<div class='Grid-cell'>" + count + "</div></div>";
             var count_num = parseInt(count);
             var percent = Math.round(count_num / denominator) * 100;
             // output += '<div class="graph"><div style="height: ' + percent + 'px;" class="bar"></div></div>';
             $("#search-history").append(output);

             classify_text(combined_query);
      }

     function classify_text(combined_query) {
         var request = $.ajax({
                url: "text-classification.php?words=" + combined_query,
                type: "get",
                dataType: "json"
            });

         // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR){
                // console.log(response);
                success: update_chart(response)
            });

    }

    function sortByProperty(property) {
        'use strict';
        return function (a, b) {
            var sortStatus = 0;
            if (a[property] < b[property]) {
                sortStatus = -1;
            } else if (a[property] > b[property]) {
                sortStatus = 1;
            }
     
            return sortStatus;
        };
    }
     


    function update_chart(data) {
        var size = Object.keys(data).length;
        var num_labels = radarChartData.labels.length;

        // myRadar.removeData();

        for (var i=0; i < data.length; i++) {
                // find the matching label
                var name = data[i]['name'];
                var indexOfLabelName = radarChartData.labels.indexOf(name);
                if (indexOfLabelName != -1) {
                    var score = data[i]['score'];
                    myRadar.datasets[0].points[indexOfLabelName].value = score;
                 }

                var msg = "'" + name + "'?'" + indexOfLabelName + "':" + score;

        }
        myRadar.update();
     };
  
   });



})();
